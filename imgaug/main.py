import utils
import rotation


DATA_ROOT = '/home/weidan/Documents/MachineLearning/Kaggle/StarFishDet'

if __name__ == "__main__":
  data_df = utils.load_meta_data(DATA_ROOT)
  img, bboxes = utils.open_image(DATA_ROOT, '0-35', data_df)
  # utils.draw_image(img, bboxes)

  rotater = rotation.Rotate(40)
  rotated_image, rotated_bboxes = rotater(img, bboxes)
  utils.draw_image(rotated_image, rotated_bboxes) 
