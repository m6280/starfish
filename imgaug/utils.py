'''
Simple util functions for testing image augmentation locally. 
Download the image files and meta data files (.csv) in your local folders,
and pass the location as 'DATA_ROOT'.

Example usage:

import utils
import rotation


DATA_ROOT = 'SET YOUR OWN PATH'
 
if __name__ == "__main__":
  data_df = utils.load_meta_data(DATA_ROOT)
  img, bboxes = utils.open_image(DATA_ROOT, '0-35', data_df)
  # utils.draw_image(img, bboxes)

  rotater = rotation.Rotate(40)
  rotated_image, rotated_bboxes = rotater(img, bboxes)
  utils.draw_image(rotated_image, rotated_bboxes) 

'''

import json
import numpy as np
import os
import pandas as pd
import tensorflow as tf

from PIL import Image, ImageDraw

import flip, rotation


def load_meta_data(data_root):
  '''
    Load the train.csv meta data.
  '''
  data_df = pd.read_csv(os.path.join(data_root, 'train.csv'))
  non_empty_df = data_df[data_df.annotations != '[]']

  return non_empty_df


def get_bboxes(image_id, data_df):
  '''
    Get the bounding boxes of an image by its id (i.e., '1-2001').
  '''
  annotations = data_df[data_df.image_id == image_id]['annotations']
  annos = json.loads(annotations[list(annotations.keys())[0]].replace('\'', '"'))
  boxes = []
  for anno in annos:
    x, y, w, h = anno['x'], anno['y'], anno['width'], anno['height']
    box = [(x, y), (x+w, y), (x, y+h), (x+w, y+h)]
    boxes.append(box)
  return boxes 


def open_image(data_root, image_id, data_df):
  '''
    Return an image object and bboxes by image id (i.e., '1-2001').
  '''
  image_path = os.path.join(data_root, 'train_images/video_%s.jpg' % image_id.replace('-', '/'))
  img = Image.open(image_path)
  bboxes = get_bboxes(image_id, data_df)
  return img, bboxes 


def draw_image(image, bboxes):
  '''
    Draw an image and its bboxes if any.
  '''
  draw = ImageDraw.Draw(image)
  for bbox in bboxes:
    draw.line([bbox[0], bbox[1]], fill='#000000', width=2)
    draw.line([bbox[0], bbox[2]], fill='#000000', width=2)
    draw.line([bbox[1], bbox[3]], fill='#000000', width=2)
    draw.line([bbox[2], bbox[3]], fill='#000000', width=2)
  image.show()
