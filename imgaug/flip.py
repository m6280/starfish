"""Flip images horizontally and/or vertically."""

from PIL import ImageOps


class Flip:
  """Flip images.

  Args:
    flip_types: type of flipping, 'horizonal' or 'vertical', default to [].

  Example:
    flipper = Flip(['horizontal'])
    flipped_images = flipper(input_images)

  """

  def __init__(self, flip_types=[]):
    self.flip_types = flip_types

  def __call__(self, images):
    flipped_images = []
    for image in images:
      if self.flip_types == 'horizontal':
        flipped = ImageOps.mirror(image)
      else:
        flipped = ImageOps.flip(image)
      flipped_images.append(flipped)
    return flipped_images
