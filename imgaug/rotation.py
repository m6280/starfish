"""Rotate images by some degrees."""

import numpy as np
from PIL import Image, ImageOps


class Rotate:
  """Rotate an image and bboxes by an angle.

  Args:
    rotate_angles: angle of rotating, default to 0.

  Example:
    rotater = Rotate(40)
    rotated_images = rotater(input_image, input_bboxes)
  """

  def __init__(self, rotate_angle=0):
    self.angle = rotate_angle

  def __call__(self, image, bboxes):
    rotated_image = image.rotate(angle=self.angle, expand=0)
    rot_angle = self.angle * np.pi / 180.
    rot_matrix = np.array([[np.cos(rot_angle), np.sin(rot_angle)], \
                           [-np.sin(rot_angle), np.cos(rot_angle)]])
    img_center = np.array([[image.size[0] / 2, -image.size[1] / 2]])
    rotated_bboxes = [] 
    for bbox in bboxes:
      bbox = np.array(bbox)
      bbox[:, 1] = -bbox[:, 1]
      bbox = bbox - img_center
      rotated_bbox = np.matmul(bbox, rot_matrix) + img_center
      rotated_bbox[:, 1] = -rotated_bbox[:, 1]
      rotated_bboxes.append(list(map(tuple, rotated_bbox)))

    return rotated_image, rotated_bboxes
